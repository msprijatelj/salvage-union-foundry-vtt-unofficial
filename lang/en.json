{
  "salvage-union": {
    "common": {
      "roll": "Roll the Die",
      "delete": "Delete",
      "edit": "Edit",
      "none": "None",
      "salvage": "Salvage",
      "documentation": "System Documentation",
      "add": "Add"
    },
    "sheets": {
      "name": "Name",
      "description": "Description",
      "no-items-of-type": "No items of this type have been added.",
      "inventory-used": "Using {count} of {max} slots",
      "add-salvage": "Add Salvage",
      "new-salvage": "Scrap",
      "new-salvage-description": "Something you picked up.",
      "table-loading": "Loading RollTable // Close and reopen sheet to view",
      "type": {
        "pilot": "Pilot",
        "mech": "Mech",
        "union-crawler": "Union Crawler",
        "equipment": "Equipment",
        "ability": "Ability",
        "chassis": "Chassis",
        "module": "Module",
        "system": "System",
        "salvage": "Salvage",
        "npc-mech": "NPC Mech",
        "npc": "NPC",
        "npc-ability": "Special Ability",
        "crawler-bay": "Crawler Bay",
        "crawler-npc": "Crawler NPC",
        "bio-titan": "Bio-Titan",
        "vehicle": "Vehicle"
      },
      "items": {
        "table-id": "Rollable Table ID",
        "edit-rollable-table": "Edit this Roll Table",
        "clear-rollable-table": "Remove this Roll Table",
        "drop-rollable-table": "Drop a Roll Table (optional)",
        "slots": "Slot Size",
        "ep": "Energy Point Cost",
        "ap": "Ability Point Cost",
        "inventory": "Uses inventory slot?",
        "cargo-slots": "Cargo Slots",
        "salvage-value": "Salvage Value",
        "level": {
          "label": "Ability Level",
          "core": "Core",
          "advanced": "Advanced",
          "legendary": "Legendary"
        },
        "traits": {
          "label": "Traits",
          "add": "Add Trait (comma-separated)",
          "remove": "Remove Trait"
        },
        "action": {
          "label": "Action Type",
          "p": "Passive",
          "f": "Free Action",
          "r": "Reaction",
          "t": "Turn Action",
          "s": "Short Action",
          "l": "Long Action",
          "d": "Downtime Action"
        },
        "range": {
          "label": "Range",
          "melee": "Melee",
          "close": "Close",
          "medium": "Medium",
          "long": "Long",
          "far": "Far",
          "trait": "Range: {range}"
        },
        "damage": {
          "label": "Damage",
          "trait": "Damage: {damage}"
        },
        "chassis": {
          "ability": "Chassis Ability",
          "ep-max": "Energy Pts.",
          "sp-max": "Structure Pts.",
          "heat-max": "Heat Cap",
          "cargo-cap": "Cargo Cap."
        },
        "crawler-bay": {
          "npc-description": "Description/Ability"
        },
        "crawler-npc": {
          "hp-minus": "Decrease current HP",
          "hp-plus": "Increase current HP"
        }
      },
      "actor": {
        "inventory": "Inventory",
        "appearance": "Appearance",
        "mech": {
          "spec": "Spec",
          "structure": "Structure Pts.",
          "energy-pts": "Energy Pts.",
          "heat-cap": "Heat Cap",
          "system-slots": "System Slots",
          "module-slots": "Module Slots",
          "chassis-type": "Chassis Type",
          "cargo": "Cargo",
          "modules": "Modules",
          "systems": "Systems",
          "ep-cost": "{cost}EP",
          "light": "Light",
          "standard": "Standard",
          "heavy": "Heavy",
          "undefined": "None",
          "roll-heat": "Roll Heat",
          "systems-modules": "Systems & Modules",
          "quirk": "Quirk"
        },
        "pilot": {
          "profile": "Profile",
          "class": "Class",
          "callsign": "Callsign",
          "background": "Background",
          "motto": "Motto",
          "keepsake": "Keepsake",
          "appearance": "Appearance",
          "stats": "Stats",
          "health": "Health",
          "ability-pts": "Ability Pts.",
          "items": "Items",
          "equipment-abilities": "Equipment & Abilities",
          "equipment": "Equipment",
          "abilities": "Abilities",
          "ap-cost": "{cost}AP",
          "roll-stress": "Roll Stress",
          "inventory-slots": "Inventory Slots",
          "skill-points": "Training Pts."
        },
        "crawler": {
          "upgrade-pool": "Upgrade Pool",
          "upkeep": {
            "label": "Upkeep Cost",
            "cost": "{level} Scrap"
          },
          "systems-bays": "Systems, Bays & NPCs",
          "bays": "Bays",
          "npcs": "NPCs",
          "type": "Crawler Type"
        },
        "npc": {
          "abilities": "Abilities",
          "hp": "HP"
        }
      }
    },
    "notifications": {
      "drop-item-rejected": "{item} items are not permitted on {actor} Actors",
      "drop-item-max": "Only one {type} item is permitted on this Actor",
      "deprecation": {
        "header": "Deprecation warning",
        "crawler-npc": "This Item Type will be removed in a future update. Crawler NPC details can now be included directly on Crawler Bay Items."
      }
    },
    "chat": {
      "send-to-chat": "Send to Chat",
      "roll-result": "Result: {roll}",
      "roll-result-target": "Target: {target} // Result: {roll}",
      "heat-roll": "Heat Check",
      "heat-pass": "<p>Pass!</p>",
      "heat-fail": "<p>Your Reactor has overloaded! Roll on the <strong>Reactor Overload Table</strong> to see what happens to your Mech.</p>"
    },
    "salvage": {
      "tech-level": "Tech Level",
      "T1": "TECH 1",
      "T2": "TECH 2",
      "T3": "TECH 3",
      "T4": "TECH 4",
      "T5": "TECH 5",
      "T6": "TECH 6"
    },
    "status": {
      "active": "Active",
      "damaged": "Damaged",
      "destroyed": "Destroyed",
      "toggle-status": "Toggle Damage"
    }
  },
  "SETTINGS": {
    "displayMechScrapTotal": {
      "label": "Display Mech Total Scrap Value",
      "hint": "Show the total of equipped Chassis, Systems and Modules, in T1 Scrap, on Mech sheets. Helpful during Mech creation."
    },
    "crawlerUpgradeScrapRequired": {
      "label": "Crawler Upgrade Scrap",
      "hint": "How many pieces of Tech Level-equivalent Scrap are required to upgrade a Crawler."
    },
    "moveItemsNotCopy": {
      "label": "Move Items between Sheets",
      "hint": "Experimental. Removes an item from the original Sheet when dragged onto on a new one."
    },
    "None": "There are no settings for Salvage Union (Unofficial)"
  }
}
