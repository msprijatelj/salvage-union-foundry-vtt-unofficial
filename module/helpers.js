/**
 * Filter a collection of Items by type, and optionally sort them
 * @param {Array} collection The list of Items
 * @param {String} type The Type of item we're looking for
 * @param {Boolean} [sort] Whether the collection should be sorted (default false)
 * @returns The filtered, sorted list
 */
export const filterItems = (collection, type, sort = false) => {
  let items = collection.filter((element) => element.type === type)

  if (sort) {
    return items.sort((a, b) => (a.name < b.name ? -1 : 1))
  }

  return items.sort((a, b) => (a.sort < b.sort ? -1 : 1))
}

/**
 * Restrict a value to within a range
 * @param {Number} value The value to restrict
 * @param {Number} min The minimum acceptable value
 * @param {Number} max The maximum acceptable value
 * @returns The clamped value
 */
export const clamp = (value, max, min = 0) =>
  Math.min(max, Math.max(min, value))

/**
 * Add a Trait to a document's system object
 * @param {Document} document The Foundry Document to be updated
 * @param {String} trait The new Trait value
 */
export const addTrait = (document, trait) => {
  if (!trait) return

  const traits = document.system.traits || []
  splitTraits(trait).map((t) => traits.push(t.trim()))

  document.update({
    system: {
      traits: traits
        .filter(onlyUnique)
        .sort((a, b) =>
          a.toLocaleLowerCase() > b.toLocaleLowerCase() ? 1 : -1,
        ),
    },
  })
}

const splitTraits = (traits) =>
  traits
    .split('//')
    .flatMap((trait) => trait.split(','))
    .filter((trait) => trait !== '')

const onlyUnique = (value, index, array) => array.indexOf(value) === index

export const removeTrait = (document, trait) => {
  const traits = document.system.traits
  traits.splice(traits.indexOf(trait), 1)
  document.update({
    system: {
      traits: traits,
    },
  })
}
