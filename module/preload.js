export const preloadHandlebarsTemplates = async () => {
  const templatePaths = [
    'systems/salvage-union/templates/partials/chassis-card.hbs',
    'systems/salvage-union/templates/partials/crawler-bay-npc.hbs',
    'systems/salvage-union/templates/partials/crawler-salvage.hbs',
    'systems/salvage-union/templates/partials/description-editor.hbs',
    'systems/salvage-union/templates/partials/inventory.hbs',
    'systems/salvage-union/templates/partials/item-card.hbs',
    'systems/salvage-union/templates/partials/item-list.hbs',
    'systems/salvage-union/templates/partials/item-rolltable.hbs',
    'systems/salvage-union/templates/partials/pilot-profile.hbs',
    'systems/salvage-union/templates/partials/pilot-stats.hbs',
    'systems/salvage-union/templates/partials/rolltable.hbs',
    'systems/salvage-union/templates/partials/salvage-card.hbs',
    'systems/salvage-union/templates/partials/salvage.hbs',
    'systems/salvage-union/templates/partials/traits.hbs',
    'systems/salvage-union/templates/sheets/items/_action_select.hbs',
    'systems/salvage-union/templates/sheets/items/_range_select.hbs',
  ]

  return loadTemplates(templatePaths)
}

export const registerSystemSettings = () => {
  game.settings.register('salvage-union', 'systemMigrationVersion', {
    config: false,
    scope: 'world',
    type: String,
    default: '',
  })

  game.settings.register('salvage-union', 'displayMechScrapTotal', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.displayMechScrapTotal.label',
    hint: 'SETTINGS.displayMechScrapTotal.hint',
    type: Boolean,
    default: true,
  })

  game.settings.register('salvage-union', 'crawlerUpgradeScrapRequired', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.crawlerUpgradeScrapRequired.label',
    hint: 'SETTINGS.crawlerUpgradeScrapRequired.hint',
    type: Number,
    default: 30,
  })

  game.settings.register('salvage-union', 'moveItemsNotCopy', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.moveItemsNotCopy.label',
    hint: 'SETTINGS.moveItemsNotCopy.hint',
    type: Boolean,
    default: false,
  })
}
