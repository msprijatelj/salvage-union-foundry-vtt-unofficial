/**
 * Roll on the RollTable associated with an Item.
 * @param {SalvageUnionItem} item The item
 * @returns Promise
 */
export const RollItemTable = async (item) => {
  const table = item.system.table
  if (table) {
    const result = await table.draw({ displayChat: false })

    const messageTemplate =
      'systems/salvage-union/templates/chat/item-table-roll.hbs'
    const templateContext = {
      name: item.name,
      result: result.results[0],
      system: item.system,
      roll: result.roll,
      activeStatus: CONFIG.SALVAGE.statusTypes.ACTIVE,
    }

    const content = await renderTemplate(messageTemplate, templateContext)
    const chatData = await buildRollChatData(result.roll, content)

    return ChatMessage.create(chatData)
  }
}

/**
 * Roll a d20 against an Actor's stress or heat value
 * @param {Integer} value The target number to roll under
 * @param {String} type stress or heat
 * @returns Promise
 */
export const CapacityRoll = async (value, type) => {
  const roll = await new Roll('1d20').roll({ async: true })

  const result = roll.total
  const fail = result <= value

  const description = fail ? `${type}-fail` : `${type}-pass`
  const messageTemplate = 'systems/salvage-union/templates/chat/roll.hbs'

  const templateContext = {
    name: `salvage-union.chat.${type}-roll`,
    value: value,
    roll: roll,
    fail: fail,
    description: description,
  }

  const content = await renderTemplate(messageTemplate, templateContext)
  const chatData = buildRollChatData(roll, content)

  return ChatMessage.create(chatData)
}

/**
 * Build common data for Roll messages in chat
 * @param {Roll} roll The Roll object
 * @param {String} content The HTML to display in the ChatMessage
 * @returns An Object to pass to ChatMessage.create()
 */
const buildRollChatData = (roll, content) => {
  return {
    user: game.user._id,
    speaker: ChatMessage.getSpeaker(),
    roll: roll,
    content: content,
    sound: CONFIG.sounds.dice,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
  }
}
