/**
 * Run all migrations
 */
export const migrateWorld = async () => {
  console.log('salvage-union | Running migrations...')

  migrateDocumentImages()
}

/**
 * Updates to document images
 * - v0.2.4: Change from .png to .webp
 */
const migrateDocumentImages = async () => {
  const pngRegex = /systems\/salvage-union\/.+\.png$/

  const worldItems = game.items.filter((item) => item.img.match(pngRegex))
  migrateDocuments(worldItems)

  const worldActors = game.actors.filter((actor) => actor.img.match(pngRegex))
  migrateDocuments(worldActors)

  game.actors.forEach((actor) => {
    const actorItems = actor.items.filter((item) => item.img.match(pngRegex))
    migrateDocuments(actorItems)
  })

  // ! This could take a while for Worlds with a lot of Scenes and Tokens
  // At least it's async...
  game.scenes.forEach((scene) => {
    const sceneTokens = scene.tokens.filter((token) =>
      token.texture.src.match(pngRegex),
    )
    for (const token of sceneTokens) {
      migrateTokenImage(token)
    }
  })
}

/**
 * Update images for a list of documents
 * @param {[ActorDocument|ItemDocument]} documents The list of Documents to update
 */
const migrateDocuments = async (documents) => {
  for (const document of documents) {
    migrateDocumentImage(document)
  }
}

/**
 * Update a single document's image
 * @param {ActorDocument|ItemDocument} document The document to update
 */
const migrateDocumentImage = async (document) => {
  console.log(
    `salvage-union | Updating img for ${document.documentName} ${document.type} ${document.id}`,
  )
  const newImg = document.img.replace(/.png$/, '.webp')
  document.update({
    img: newImg,
  })
}

/**
 * Update a Token's image
 * @param {TokenDocument} token
 */
const migrateTokenImage = async (token) => {
  console.log(
    `salvage-union | Updating Token for Actor ${token.actorId} in Scene ${token.parent.name}`,
  )
  const newImg = token.texture.src.replace(/.png$/, '.webp')
  token.update({
    texture: {
      src: newImg,
    },
  })
}
