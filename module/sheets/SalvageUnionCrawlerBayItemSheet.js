import SalvageUnionItemSheet from './SalvageUnionItemSheet.js'

export default class SalvageUnionCrawlerBayItemSheet extends SalvageUnionItemSheet {
  get template() {
    return 'systems/salvage-union/templates/sheets/items/crawler-bay-sheet.hbs'
  }
}
