import SalvageUnionItemSheet from './SalvageUnionItemSheet.js'

export default class SalvageUnionChassisItemSheet extends SalvageUnionItemSheet {
  get template() {
    return 'systems/salvage-union/templates/sheets/items/chassis-sheet.hbs'
  }
}
