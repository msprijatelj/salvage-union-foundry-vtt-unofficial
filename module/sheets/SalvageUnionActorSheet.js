import * as Dice from '../dice.js'
import { fieldToChat } from '../chat.js'
import { clamp, addTrait, removeTrait } from '../helpers.js'

export default class SalvageUnionActorSheet extends ActorSheet {
  get template() {
    return `systems/salvage-union/templates/sheets/actors/${this.actor.type}-sheet.hbs`
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 1000,
      height: 750,
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.tab-content',
          initial: 'main',
        },
      ],
    })
  }

  /**
   * Run validation on an Item before adding it to an Actor's sheet.
   * @param {Event} event The drop event - used for calling super
   * @param {Object} data Data about the dropped item
   * @returns
   */
  async _onDropItem(event, data) {
    if (data.type === 'Item') {
      const item = await fromUuid(data.uuid)
      const itemType = game.i18n.localize(
        `salvage-union.sheets.type.${item.type}`,
      )
      const actorType = game.i18n.localize(
        `salvage-union.sheets.type.${this.actor.type}`,
      )

      const permitted = CONFIG.SALVAGE.permittedItems[this.actor.type].includes(
        item.type,
      )
      if (permitted) {
        if (item.type === 'chassis' && this.actor.system.chassis) {
          ui.notifications.error(
            game.i18n.format('salvage-union.notifications.drop-item-max', {
              type: itemType,
            }),
          )
          return
        }
        await super._onDropItem(event, data)

        const moveNotCopy = game.settings.get(
          'salvage-union',
          'moveItemsNotCopy',
        )
        if (
          item.parent &&
          moveNotCopy &&
          item.parent.uuid !== this.actor.uuid
        ) {
          item.parent.deleteEmbeddedDocuments('Item', [item.id])
        }
        return
      } else {
        ui.notifications.error(
          game.i18n.format('salvage-union.notifications.drop-item-rejected', {
            item: itemType,
            actor: actorType,
          }),
        )
        return
      }
    }
    super._onDropItem(event, data)
  }

  /**
   * Context menu for editing and deleting items from the sheet
   */
  itemContextMenu = [
    {
      name: game.i18n.localize('salvage-union.common.edit'),
      icon: '<i class="fas fa-edit"></i>',
      callback: (element) => {
        let itemId = element.closest('.item-context-menu').data('item-id')
        this.actor.items.get(itemId).sheet.render(true)
      },
    },
    {
      name: game.i18n.localize('salvage-union.common.delete'),
      icon: '<i class="fas fa-trash"></i>',
      callback: (element) => {
        let itemId = element.closest('.item-context-menu').data('item-id')
        this.actor.deleteEmbeddedDocuments('Item', [itemId])
      },
    },
  ]

  getData() {
    const context = super.getData()
    context.config = CONFIG.SALVAGE
    context.activeStatus = CONFIG.SALVAGE.statusTypes.ACTIVE

    context.system = this.actor.system

    context.displayMechScrapTotal =
      this.actor.type === 'mech'
        ? game.settings.get('salvage-union', 'displayMechScrapTotal')
        : false

    context.crawlerUpgradeScrapRequired =
      this.actor.type === 'union-crawler'
        ? game.settings.get('salvage-union', 'crawlerUpgradeScrapRequired')
        : 0

    return context
  }

  activateListeners(html) {
    if (this.isEditable) {
      if (this.actor.isOwner) {
        new ContextMenu(html, '.item-context-menu', this.itemContextMenu)

        html.find('.rollable-table-roll').click(this._rollItemTable.bind(this))

        if (this.actor.type === 'mech') {
          html.find('.capacityRoll').click(this._rollCapacity.bind(this))
        }
        if (
          ['mech', 'npc-mech', 'union-crawler', 'vehicle'].includes(
            this.actor.type,
          )
        ) {
          html.find('.toggleDamage').click(this._toggleDamage.bind(this))
        }
        if (this.actor.type === 'union-crawler') {
          html
            .find('.crawler-npc-hp')
            .click(this._adjustCrawlerNpcHealth.bind(this))
        }

        if (['pilot', 'mech'].includes(this.actor.type)) {
          html.find('.sendFieldToChat').click(this._sendFieldToChat.bind(this))
          html.find('.rollProfile').click(this._rollProfileField.bind(this))
        }

        html.find('.add-salvage').click(this._addSalvage.bind(this))
      }
    }

    if (['npc', 'vehicle'].includes(this.actor.type)) {
      html.find('.add-trait').click(this._onTraitCreate.bind(this))
      html.find('.trait-name').keydown(this._onTraitCreate.bind(this))
      html.find('.remove-trait').click(this._onTraitDelete.bind(this))
    }

    html.find('.sendToChat').click(this._sendItemToChat.bind(this))

    super.activateListeners(html)
  }

  /**
   * Roll on the table associated with an Item
   * @param {Event} event The triggering event
   */
  _rollItemTable(event) {
    event.preventDefault()
    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    Dice.RollItemTable(item)
  }

  /**
   * Post an item into the Chat
   * @param {Event} event The triggering event
   */
  _sendItemToChat(event) {
    event.preventDefault()
    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    item.toChat()
  }

  _sendFieldToChat(event) {
    event.preventDefault()
    const field = event.currentTarget.dataset.fieldName
    const value = this.actor.system[field]

    if (!value) return

    fieldToChat(
      game.i18n.localize(`salvage-union.sheets.actor.pilot.${field}`),
      value,
    )
  }

  /**
   * Roll a d20 vs. current Heat and post the result
   * @param {Event} event The triggering event
   */
  _rollCapacity(event) {
    event.preventDefault()
    const target = event.currentTarget.dataset
    Dice.CapacityRoll(target.value, target.type)
  }

  /**
   * Change a mech's System or Module repair status
   * @param {Event} event The triggering event
   */
  _toggleDamage(event) {
    event.preventDefault()
    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    let newStatus =
      (item.system.status || CONFIG.SALVAGE.statusTypes.ACTIVE) + 1
    if (newStatus > CONFIG.SALVAGE.statusTypes.DESTROYED)
      newStatus = CONFIG.SALVAGE.statusTypes.ACTIVE
    item.update({
      system: {
        status: newStatus,
      },
    })
  }

  /**
   * Change a Crawler NPC's health
   * @param {Event} event The triggering event
   */
  _adjustCrawlerNpcHealth = (event) => {
    event.preventDefault()
    const dataset = event.currentTarget.dataset
    const change = parseInt(dataset.change)

    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    if (!item) return

    const value = item.system.npc_health.value + change
    const max = item.system.npc_health.max

    item.update({
      system: {
        npc_health: {
          value: clamp(value, max),
        },
      },
    })
  }

  /**
   * Remove an Item from the Actor
   * @param {Event} event The triggering event
   */
  _deleteItem(event) {
    event.preventDefault()
    const dataset = event.currentTarget.dataset
    let itemId = dataset.itemId
    this.actor.deleteEmbeddedDocuments('Item', [itemId])
  }

  /**
   * Add a Trait
   * @param {Event} event
   */
  _onTraitCreate(event) {
    const newTrait = $(event.currentTarget.closest('.traits'))
      .find('.trait-name')[0]
      .value.trim()

    if (event.type === 'keydown') {
      // Trigger the action on enter
      if (event.keyCode === 13) addTrait(this.actor, newTrait)
    } else {
      // Fallback for other events calling this method
      addTrait(this.actor, newTrait)
    }
  }

  /**
   * Remove a Trait from the list
   * @param {Event} event
   */
  _onTraitDelete(event) {
    event.preventDefault()
    const traitToRemove = event.currentTarget.dataset.trait
    removeTrait(this.actor, traitToRemove)
  }

  /**
   * Add an empty Salvage entry
   * @param {Event} event
   */
  async _addSalvage(event) {
    event.preventDefault()
    const itemType = 'salvage'
    const defaultData = {
      techLevel: 'T1',
    }

    const itemData = {
      name: game.i18n.localize('salvage-union.sheets.new-salvage'),
      type: itemType,
      data: {
        description: game.i18n.localize(
          'salvage-union.sheets.new-salvage-description',
        ),
        ...defaultData,
      },
    }

    const docs = await this.actor.createEmbeddedDocuments('Item', [itemData])

    docs.forEach((salvage) => salvage.sheet.render(true))
  }

  async _rollProfileField(event) {
    event.preventDefault()
    const fieldName = event.currentTarget.dataset.field
    let result = ''

    switch (fieldName) {
      case 'motto':
        result = await this._rollTable('TnCUB0IpTOeKTcUD')
        break
      case 'keepsake':
        result = await this._rollTable('oNmW8N3ZZRefBEQB')
        break
      case 'background':
        result = await this._rollTable('qLjj3SFXVKK2FAVg')
        break
      case 'quirk':
        result = await this._rollTable('yzNQCYbXgsZV8Fdi')
        break
      case 'appearance':
        result = await this._rollTable('HHDgxAYMKLysRS9k')
        break
      default:
        break
    }

    this.actor.update({
      system: {
        [fieldName]: result,
      },
    })
  }

  async _rollTable(tableId) {
    const table = await fromUuid(`Compendium.salvage-union.tables.${tableId}`)
    const result = await table.draw({ displayChat: false })

    return result?.results[0]?.text
  }
}
