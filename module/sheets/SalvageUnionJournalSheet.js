export default class SalvageUnionJournalSheet extends JournalSheet {
  static get defaultOptions() {
    let classes = [
      'sheet journal-sheet journal-entry salvage-union-journal-sheet',
    ]
    return mergeObject(super.defaultOptions, {
      classes: classes,
    })
  }
}
