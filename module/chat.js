/**
 * Send a basic label/value to the chat log
 * @param {String} label The field label
 * @param {String} value The field value
 * @returns
 */
export const fieldToChat = async (label, value) => {
  const messageTemplate = 'systems/salvage-union/templates/chat/wrapper.hbs'
  const templateContext = {
    content: `<h3>${label}</h3><p>${value}</p>`,
  }

  const chatData = {
    user: game.user._id,
    speaker: ChatMessage.getSpeaker(),
    content: await renderTemplate(messageTemplate, templateContext),
  }

  return ChatMessage.create(chatData)
}

/**
 * Callbacks for rendering a message
 * @param {ChatMessage} _message Message object (unused)
 * @param {HTMLElement} html Message HTML body
 * @param {Object} _data Message metadata (unused)
 * @returns
 */
export const onRenderChatMessage = async (_message, html, _data) => {
  if (html.find('.salvage-union')[0]) return null
  html.find('.message-content').wrapInner('<div class="salvage-union"></div>')
}
