import { SALVAGE } from './module/config.js'
import SalvageUnionActor from './module/SalvageUnionActor.js'
import SalvageUnionItem from './module/SalvageUnionItem.js'
import SalvageUnionItemSheet from './module/sheets/SalvageUnionItemSheet.js'
import SalvageUnionChassisItemSheet from './module/sheets/SalvageUnionChassisItemSheet.js'
import SalvageUnionCrawlerBayItemSheet from './module/sheets/SalvageUnionCrawlerBayItemSheet.js'
import SalvageUnionActorSheet from './module/sheets/SalvageUnionActorSheet.js'
import SalvageUnionNPCActorSheet from './module/sheets/SalvageUnionNPCActorSheet.js'
import SalvageUnionJournalSheet from './module/sheets/SalvageUnionJournalSheet.js'
import {
  preloadHandlebarsTemplates,
  registerSystemSettings,
} from './module/preload.js'
import * as Migrations from './module/migrate.js'
import * as Chat from './module/chat.js'

Hooks.once('init', function () {
  console.log(
    'salvage-union | Initialising Salvage Union (Unofficial) system...',
  )
  // Register system settings
  registerSystemSettings()

  CONFIG.SALVAGE = SALVAGE
  CONFIG.Item.documentClass = SalvageUnionItem
  CONFIG.Actor.documentClass = SalvageUnionActor

  CONFIG.SALVAGE.crawlerUpgradeScrapRequired = game.settings.get(
    'salvage-union',
    'crawlerUpgradeScrapRequired',
  )

  // Item sheets
  Items.registerSheet('salvage-union', SalvageUnionItemSheet, {
    makeDefault: true,
  })
  Items.registerSheet('salvage-union', SalvageUnionChassisItemSheet, {
    makeDefault: true,
    types: ['chassis'],
  })
  Items.registerSheet('salvage-union', SalvageUnionCrawlerBayItemSheet, {
    makeDefault: true,
    types: ['crawler-bay'],
  })
  Items.unregisterSheet('core', ItemSheet)

  // Actor sheets
  Actors.registerSheet('salvage-union', SalvageUnionActorSheet, {
    makeDefault: true,
    types: ['pilot', 'mech', 'union-crawler', 'bio-titan'],
  })
  Actors.registerSheet('salvage-union', SalvageUnionNPCActorSheet, {
    makeDefault: true,
    types: ['npc', 'npc-mech', 'vehicle'],
  })
  Actors.unregisterSheet('core', ActorSheet)

  // Journal sheet
  Journal.registerSheet('salvage-union', SalvageUnionJournalSheet, {
    makeDefault: true,
  })

  // Preload templates
  preloadHandlebarsTemplates()

  // Handlebar helpers
  Handlebars.registerHelper('range', function (range) {
    if (range[0] === range[1]) return range[0]
    return '' + range[0] + ' - ' + range[1]
  })

  Handlebars.registerHelper('addInlineCosts', function (description) {
    const cost_regex = /{cost=([\dX]+[EA]P)}/g // e.g. {cost=3EP}
    if (description)
      return description.replace(
        cost_regex,
        '<span class="item-cost">$1</span>',
      )
    return null
  })

  Handlebars.registerHelper('trPath', (path, key) => path + '.' + key)
  Handlebars.registerHelper(
    'statusClass',
    (status) => CONFIG.SALVAGE.statusClasses[status],
  )
  Handlebars.registerHelper(
    'statusLabel',
    (status) => CONFIG.SALVAGE.statusLabels[status],
  )
  Handlebars.registerHelper('enrichHTML', (html) =>
    TextEditor.enrichHTML(html, { async: false }),
  )
  Handlebars.registerHelper('not', (condition) => !condition)
  Handlebars.registerHelper('arrayContains', (array, element) => {
    return array.includes(element)
  })
  Handlebars.registerHelper('join', (array, separator) => array.join(separator))
  Handlebars.registerHelper('inventoryItemHeight', (slots) => {
    slots = parseInt(slots)
    return `calc(${slots * 1.5}rem + ${(slots - 1) * 0.5}rem + ${slots * 2}px)`
  })
  Handlebars.registerHelper('times', (n, content) => {
    let result = ''
    for (let i = 0; i < n; i++) {
      content.data.index = i + 1
      result += content.fn(i)
    }
    return result
  })
  Handlebars.registerHelper('any', (array) => array.length > 0)
  Handlebars.registerHelper('crawlerUpkeepCost', (techLevel) =>
    game.i18n.format('salvage-union.sheets.actor.crawler.upkeep.cost', {
      level: techLevel,
    }),
  )

  // TinyMCE styling
  CONFIG.TinyMCE.content_css = 'systems/salvage-union/styles/tinymce.css'
  CONFIG.TinyMCE.style_formats.push({
    title: 'Salvage Union',
    items: [{ title: 'Condensed', inline: 'span', classes: 'condensed' }],
  })
})

Hooks.once('ready', () => {
  if (!game.user.isGM) return

  const currentVersion = game.settings.get(
    'salvage-union',
    'systemMigrationVersion',
  )
  const NEEDS_MIGRATION_VERSION = '0.2.4'

  const needsMigration =
    !currentVersion || isNewerVersion(NEEDS_MIGRATION_VERSION, currentVersion)

  if (needsMigration) {
    Migrations.migrateWorld()
    game.settings.set(
      'salvage-union',
      'systemMigrationVersion',
      NEEDS_MIGRATION_VERSION,
    )
  }
})

Hooks.on('renderSettings', (_app, html) => {
  const url =
    'https://gitlab.com/pacosgrove1/salvage-union-foundry-vtt-unofficial/-/wikis/home'
  const label = game.i18n.localize('salvage-union.common.documentation')

  let helpButton = $(
    `<button id="salvage-union-help-btn" data-action="salvage-union-help">
      <i class="fas fa-gear"></i> ${label}
    </button>`,
  )
  html.find('button[data-action="support"]').before(helpButton)

  helpButton.on('click', (ev) => {
    ev.preventDefault()
    window.open(url, 'salvage-union-help', 'width=1032,height=720')
  })
})

Hooks.on('renderChatMessage', Chat.onRenderChatMessage.bind(this))

Hooks.once('diceSoNiceReady', (dice3d) => {
  const commonDice = {
    category: 'Salvage Union',
    foreground: '#FFFFFF',
    outline: '#000000',
    visibility: 'visible',
    font: 'Roboto Regular',
    fontScale: { d20: '0.75' },
    material: 'metal',
    texture: 'metal',
  }

  dice3d.addColorset(
    {
      ...commonDice,
      name: 'salvage-orange',
      description: 'Corpo Orange',
      background: '#f68a50',
      edge: '#f68a50',
    },
    'default',
  )

  dice3d.addColorset(
    {
      ...commonDice,
      name: 'salvage-blue',
      description: 'Salvager Blue',
      background: '#6bcef3',
      edge: '#6bcef3',
    },
    'default',
  )

  dice3d.addSystem(
    {
      id: 'salvage-union',
      name: 'Salvage Union',
    },
    'preferred',
  )

  dice3d.addDicePreset({
    type: 'd20',
    labels: [
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      '11',
      '12',
      '13',
      '14',
      '15',
      '16',
      '17',
      '18',
      '19',
      'systems/salvage-union/images/d20.webp',
    ],
    bumpMaps: [
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      'systems/salvage-union/images/d20-b.webp',
    ],
    system: 'salvage-union',
  })
})
