[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B11L4PE)

# Salvage Union

A [FoundryVTT](https://foundryvtt.com) System for running [Salvage Union](https://salvage-union.backerkit.com/hosted_preorders) by [Leyline Press](https://leyline.press/).

![Screenshot of the Pilot and Mech actor sheets in Foundry](https://i.imgur.com/A8KHUzo.png)

## Installation

### Automatic

1. Go to the **Game Systems** tab of the Foundry Setup screen.
2. Click the **Install System** button.
3. Search for `Salvage Union` in the Filter Packages text box.
4. Click the Install button next to the **Salvage Union (Unofficial)** entry.

### Manual

1. Find the desired version on the [Releases page](https://gitlab.com/pacosgrove1/salvage-union-foundry-vtt-unofficial/-/releases).
2. Download the `salvage-union-vx.x.x.zip` file for the chosen version.
3. Unzip the file to your FoundryVTT Data `systems` folder (see [here](https://foundryvtt.com/article/configuration/#where-user-data) for details).
4. Close and reopen FoundryVTT, if required.

## Features

- Player sheets for Pilots and Mechs
- NPC sheets for People/Creatures, Mechs, Vehicles and Bio-Titans
- Rollable Tables attach directly to Items for rolling from the character sheet
- Compendiums for Mech Chassis, Systems and Modules, Pilot Abilities and Equipment, and Union Crawler Bays
- Compendiums of core rules, Rollable Tables and Macros
- Custom d20s for [Dice So Nice!](https://gitlab.com/riccisi/foundryvtt-dice-so-nice)

## Usage

Usage instructions can be found on the [project wiki](https://gitlab.com/pacosgrove1/salvage-union-foundry-vtt-unofficial/-/wikis/home) (still a work in progress).

## Credits

Compendiums contain text from the Salvage Union Core Rulebook, reproduced under licence from Leyline Press for use in FoundryVTT only.

The Roboto family of fonts are licensed under the Apache License, Version 2.0. For more details, see [fonts.google.com/specimen/Roboto](https://fonts.google.com/specimen/Roboto).

Icons from [Game-Icons.net](https://www.game-icons.net) licensed under [CC-BY-3.0](http://creativecommons.org/licenses/by/3.0/).

- _Auto repair_, _Brain_, _Cogsplosion_, _Gluttonous smile_, _Internal injury_, _Lightning spanner_, _Missile mech_, _Pokecog_, _Processor_, _Robot golem_ and _Run_ by [Lorc](https://lorcblog.blogspot.com)
- _Character_, _Delivery drone_, _Dice 20 faces 20_, _Pipes_, _Radar cross section_ and _Shrug_ by [Delapouite](https://delapouite.com)
- _Reactor_ by [sbed](http://opengameart.org/content/95-game-icons)
- _Jeep_ and _Tank tread_ by [Skoll](https://game-icons.net)
- _Acrobatic_ by [DarkZaitzev](http://darkzaitzev.deviantart.com)

## Disclaimer

This project is not endorsed by or affiliated with Leyline Press, and is for personal use only.

This work uses material from the Quest Creators Resource. The Quest Creators Resource by The Adventure Guild, LLC, is licensed under CC BY 4.0.

Salvage Union is © 2023 Leyline Press Ltd. All Rights Reserved.
