# Changelog

## v1.0.2

- Fix Compendium Journal links

## v1.0.1

- Fix pack inclusion in CI build

## v1.0.0

- Combine Crawler Bay and Crawler NPC Items
  - **Note:** Crawler NPCs will be removed in a future update
- Remove FoundryVTT v10 comptibility
- Update Compendiums to v11 format, with native folders
- New experimental option to allow moving of items between Actors
  - Dragging an item from one sheet to another will also remove it from the source Actor
- Multiple Traits can now be entered at once on supported sheets
  - Separated by `,` or `//`

## v0.2.7

- Rework Item/Actor ownership permissions
- Fix inventory overflow display

## v0.2.6

- FoundryVTT v11 compatibility
- Add buttons to roll Pilot Background, Motto and Keepsake
- Add Mech Quirk and Appearance to sheet, with roll buttons
- Add Union Crawler Upkeep and Upgrade tracking
- Improve Crawler salvage management
- Journal and translation fixes

## v0.2.5

- Improved Bio-Titan, Vehicle and NPC sheets
- Improved Inventory management for Pilots and Mechs
- Added compendia of Chassis, Systems, Modules, Pilot Abilities, Equipment, etc.
- Journal styling

## v0.2.4

- Add Bio-Titan Actor
- Add Tagging for Items
- Bug fixes around RollableTables attached from a Compendium

## v0.2.3

- No functionality changes, just code cleanup and some icon colour adjustment.

## v0.2.2

- Add functionality to adjust Crawler NPC HP from the Crawler sheet
- Add `clamp` helper method
- Use arrow functions for helpers

## v0.2.1

- Fix mech Chassis sheet
- Correctly display Chassis Ability on owner Mech sheet
- Enrich HTML for item descriptions to ensure dropped Document links work

## v0.2.0

- Updated for FoundryVTT v10
- System changes based on Alpha WIP rulebook
